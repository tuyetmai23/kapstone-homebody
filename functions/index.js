const functions = require("firebase-functions");
const axios = require("axios");
//https://stackoverflow.com/questions/42755131/enabling-cors-in-cloud-functions-for-firebase
const cors = require("cors")({ origin: true });

//https://firebase.google.com/docs/functions/write-firebase-functions

exports.youtube = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    const search = encodeURI(request.query.q);
    const results = await axios.get(
      `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&q=${search}&videoDuration=videoDurationUnspecified&key=${
        functions.config().youtube.key
      }`
    );

    const videos = results.data.items.map((video) => {
      if (video.id.kind === "youtube#video") {
        return {
          title: video.snippet.title,
          videoURL: `https://www.youtube.com/embed/${video.id.videoId}`,
          thumbnail: video.snippet.thumbnails.medium.url,
        };
      }
    });

    const randomNum = Math.floor(Math.random() * videos.length);

    response.json(videos[randomNum]);

    
  });
});
