
[HOMEBODY]
Homebody is the video workout roulette app of your dreams!
You have the option of a cardio workout, a yoga video or if you are feeling fun a prancercise routine. 
If you do not like your original three options, hit that roulette button to find something new. 
Stop by our chatty chat to give your Homebody homies shout outs of encouragemet! 

We hope to expand our app in the future with more funding. For now, enjoy working out your post self quarantine body with Homebody!

Project Requirements:

Once your project has been completed please fill out the links below. These are a part of the rubric and must be completed before the submission deadline
Scrum Board URL | https://trello.com/b/gG3BJ4Po/kapstone
Deployment URL | https://kapstone-13bac.web.app/
Kapstone Pitch URL | https://docs.google.com/document/d/17RzJdcpXsP1xsnH0er6lc85C_ifzCbwzHawocfdOOGo/edit
Wire Frame | https://teamhomebody.invisionapp.com/
Group Retrospective Document URL | https://funretro.io/publicboard/NzPwTbGDMZXw5naPJnKZbM8m7hg2/f519db06-56c9-4bfe-906b-35df58fa5133
Kapstone Feedback Form | https://docs.google.com/forms/d/1yeIyQH6ZE6y5Z0qB2i8yW5_1Gzfxs8YiJsNlcyjR0WA/viewform?edit_requested=true

Resources
We got coaching and help from the following super friends:
Vince
TJ
Jake
Nico
Gabby 
Marcel
Chris Jones - A resource outside of Kenzie and a generally swell guy

Aside from the Google Machine at large, our code got assistance from the following sites:
https://fireship.io/lessons/react-firebase-chat-app-tutorial/
https://www.youtube.com/watch?v=PKwu15ldZ7k&t=1479s
https://www.robinwieruch.de/react-hooks-fetch-data
https://artisansweb.net/get-youtube-video-list-by-keywords-using-youtube-search-api-and-javascript/
https://blog.logrocket.com/build-a-youtube-video-search-app-with-angular-and-rxjs/
https://medium.com/trabe/react-useref-hook-b6c9d39e2022
https://reactjs.org/docs/handling-events.html
https://medium.com/@lucymarmitchell/using-then-catch-finally-to-handle-errors-in-javascript-promises-6de92bce3afc
https://www.npmjs.com/package/axios


 

Note:  Points will be deducted if this is not filled in correctly. Also each one of these MUST live in their own document. For example DO NOT put your retrospective in your scrum board as it will not be counted and you will lose points.


About Us
Tell us about your team, who was involved, what role did they fulfill and what did they work on (the more details the better). Also please choose ONE of the staff below that was your investor and remove the others.



Product Investor - TJ
QA and Developers - Mai(master of code)
QA and dev - Matt(styling guru yo)
Product Owner - Elisia(fearless leader)
Scrum Master - Kamela(multiple hat wearer)









This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
