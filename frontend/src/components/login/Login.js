import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import cheetahprint from "../../assets/cheetahBackground.jpg";
import "./login.css";

export const Login = () => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const { login } = useAuth();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      setError("");
      setLoading(true);
      await login(emailRef.current.value, passwordRef.current.value);
      history.push("/profile");
    } catch {
      setError("Failed to sign in");
      setLoading(false);
    }
  };

  return (
    <>
      <div id="login-card">
        <Card>
          <Card.Body
            style={{
              backgroundImage: `url(${cheetahprint})`,
              backgroundSize: "cover",
            }}
          >
            <h2 className="text-center mb-4"> LOGIN </h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label className="email">Email</Form.Label>
                <Form.Control
                  style={{ backgroundColor: "#e9ecef" }}
                  type="email"
                  ref={emailRef}
                  required
                />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label className="password">Password</Form.Label>
                <Form.Control
                  style={{ backgroundColor: "#e9ecef" }}
                  type="password"
                  ref={passwordRef}
                  required
                />
              </Form.Group>

              <Button id="login-btn" disabled={loading} type="submit">
                Log In
              </Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
          Dont have a Account? <Link to="/signup">Sign Up</Link>
        </div>
      </div>
    </>
  );
};
