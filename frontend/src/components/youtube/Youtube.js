import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button } from "react-bootstrap";
import "./youtube.css"

export const Youtube = (props) => {
    const [videoData, setVideoData] = useState({
        title: "",
        url: "",
        thumbnail: ""
    })
    const [error, setError] = useState();

    const youtubeCall = () => {
     
        axios.get(`https://us-central1-kapstone-13bac.cloudfunctions.net/youtube?q=${props.searchTerm}`)
            .then(res => {
                setVideoData(res.data);
            })
            .catch(() => {
                setError("Video failed to upload");
            })
    }
    useEffect(() => {
        youtubeCall()

    }, [props.searchTerm])

    function handleClick(e) {
        e.preventDefault();
        youtubeCall()

    }

    return (
        
        <div className="video-container">
            <h3 id="video-title">{props.title}</h3>
            <iframe
                title={videoData.title}
                width='426'
                height='240'
                src={videoData.videoURL}
                frameBorder="0"
                allowFullScreen>
            </iframe>
            <Button id="roulette-button" 
            onClick={handleClick}>Roulette!</Button>
        </div>
      
    )

}
