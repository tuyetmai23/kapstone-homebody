import React from "react";
import pinkpants from "../../assets/weightlift.png";
import "./landingPage.css";

export const LandingPage = () => {
  return (
    <div
      id="main-container"
      style={{ backgroundImage: `url(${pinkpants})`, backgroundSize: "cover" }}
    >
      <h2 id="masthead">HOMEBODY</h2>
    </div>
  );
};
