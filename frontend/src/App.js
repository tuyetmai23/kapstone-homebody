import React from "react";
import { Navigation } from "./components";

export const App = () => {
  return <Navigation />;
};

export default App;
