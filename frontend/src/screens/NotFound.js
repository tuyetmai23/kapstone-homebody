import React from "react";
import fonda404 from "../assets/fonda404.png"
import { Link } from "react-router-dom"
import "./screens.css"

export const NotFoundScreen = () => {
  return (
    <>
      <div id="not-found-page" style={{
              backgroundImage: `url(${fonda404})`,
              backgroundSize: "cover",
            }}>
        <br />
        <h1 id="not-found-text">
          You've somehow ended up in a bad 80's aerobic timewarp!
          <br />
          Find your way back or be stuck here forever!{" "}
        </h1>
        <br />
        <br />
        <Link id="go-back-btn" to="/profile" >
            GET <br /> ME <br /> OUTTA <br /> HERE!!!
          </Link>
      </div>
    </>
  );
};
